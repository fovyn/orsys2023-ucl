package be.bstorm.formation.orsys2023ucljpa.utils;

import be.bstorm.formation.orsys2023ucljpa.config.jwt.JwtConfig;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.function.Function;

@Component
public class JwtHelper {
    private final JwtConfig config;
    private JwtParser parser;
    private JwtBuilder builder;
    @Autowired
    public JwtHelper(JwtConfig jwtConfig) {
        config = jwtConfig;
        parser = Jwts.parserBuilder().setSigningKey(config.getKey()).build();
        builder = Jwts.builder().signWith(config.getKey());
    }

    public String getUsernameFromToken(String token) {
        return getClaimFormToken(token, Claims::getSubject);
    }

    public Date getExpireInFromToken(String token) {
        return getClaimFormToken(token, Claims::getExpiration);
    }

    public boolean isExpire(String token) {
        Date expireDate = getExpireInFromToken(token);

        return expireDate.before(new Date());
    }

    public Collection<String> getAuthoritiesFromToken(String token) {
        return getClaimFormToken(token, (claims) -> claims.get("roles", List.class));
    }


    public <T> T getClaimFormToken(String token, Function<Claims, T> claimResolver) {
        Claims claims = getClaimsFormToken(token);

        return claimResolver.apply(claims);
    }

    private Claims getClaimsFormToken(String token) {
        return parser.parseClaimsJws(token).getBody();
    }

    public boolean validateToken(String token, UserDetails userDetails) {
        String username = getUsernameFromToken(token);
        boolean isExpired = isExpire(token);

        return userDetails.getUsername().equals(username) && !isExpired;
    }


    public String generateToken(UserDetails userDetails) {
        Map<String, Object> claims = new HashMap<>();

        claims.put("roles", userDetails.getAuthorities().stream().map(GrantedAuthority::getAuthority).toList());


        return generateToken(userDetails, claims);
    }

    private String generateToken(UserDetails ud, Map<String, Object> claims) {
        return builder
                .setSubject(ud.getUsername())
                .setIssuedAt(new Date())
                .setExpiration(new Date(new Date().getTime() + config.getExpireInMillis()))
//                .setClaims(claims)
                .compact();
    }
}
