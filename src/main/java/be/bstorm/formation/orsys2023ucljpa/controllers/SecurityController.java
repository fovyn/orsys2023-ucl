package be.bstorm.formation.orsys2023ucljpa.controllers;

import be.bstorm.formation.orsys2023ucljpa.models.security.LoginDto;
import be.bstorm.formation.orsys2023ucljpa.models.security.Token;
import be.bstorm.formation.orsys2023ucljpa.utils.JwtHelper;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class SecurityController {

    private final UserDetailsService userDetailsService;
    private final PasswordEncoder passwordEncoder;
    private final JwtHelper jwtHelper;

    @PostMapping("/login")
    public ResponseEntity<Token> loginAction(
            @Valid @RequestBody LoginDto loginDto
    ) {
        UserDetails userDetails = userDetailsService.loadUserByUsername(loginDto.username());

        if (!passwordEncoder.matches(loginDto.password(), userDetails.getPassword())) {
            throw new RuntimeException();
        }

        return ResponseEntity.ok(new Token(jwtHelper.generateToken(userDetails)));
    }
}
