package be.bstorm.formation.orsys2023ucljpa.config.jwt;

import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.crypto.SecretKey;

@Component
@ConfigurationProperties(prefix = "jwt")
@Data
public class JwtConfig {
    private int expireInMillis = 84600000;
    private SecretKey key = Keys.secretKeyFor(SignatureAlgorithm.HS512);
}
