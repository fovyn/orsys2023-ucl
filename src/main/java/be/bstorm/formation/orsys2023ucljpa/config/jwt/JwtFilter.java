package be.bstorm.formation.orsys2023ucljpa.config.jwt;

import be.bstorm.formation.orsys2023ucljpa.utils.JwtHelper;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class JwtFilter extends OncePerRequestFilter {
    private final JwtHelper jwtHelper;
    private final UserDetailsService userDetailsService;

    @Override
    protected void doFilterInternal(
            HttpServletRequest request,
            HttpServletResponse response,
            FilterChain filterChain
    ) throws ServletException, IOException {
        String authorization = request.getHeader("Authorization");
        if (authorization != null) {
            if (!authorization.contains("Bearer ")) throw new RuntimeException();

            String token = authorization.substring(7);

            if (token.isEmpty()) throw new RuntimeException();
            String username = jwtHelper.getUsernameFromToken(token);

            if (username == null || username.isEmpty()) throw new RuntimeException();

            UserDetails userDetails = userDetailsService.loadUserByUsername(username);
            if (userDetails != null) {

                if (!jwtHelper.validateToken(token, userDetails)) {
                    throw new RuntimeException();
                }

                UsernamePasswordAuthenticationToken upat = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());

                upat.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(upat);
            }
        }



        filterChain.doFilter(request, response);
    }
}
