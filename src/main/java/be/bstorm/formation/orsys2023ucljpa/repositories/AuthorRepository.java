package be.bstorm.formation.orsys2023ucljpa.repositories;

import be.bstorm.formation.orsys2023ucljpa.entities.Author;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.List;

@RepositoryRestResource
public interface AuthorRepository extends JpaRepository<Author, Long> {

    @Override
    Page<Author> findAll(Pageable pageable);
}
