package be.bstorm.formation.orsys2023ucljpa.repositories;

import be.bstorm.formation.orsys2023ucljpa.entities.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface BookRepository extends JpaRepository<Book, Long> {
}
