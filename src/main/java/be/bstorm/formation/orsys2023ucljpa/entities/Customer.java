package be.bstorm.formation.orsys2023ucljpa.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Entity
@Data
public class Customer extends Person {
    @Column(nullable = false)
    private String firstname;
    @Column(nullable = false)
    private LocalDate birthDate;

    @OneToMany(mappedBy = "customer")
    private List<Borrow> borrows;
}
