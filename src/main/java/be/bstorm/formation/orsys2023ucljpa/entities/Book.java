package be.bstorm.formation.orsys2023ucljpa.entities;

import jakarta.persistence.*;
import lombok.Data;

import java.util.List;

@Entity
@Table(uniqueConstraints = {
        @UniqueConstraint(name = "UK_Book_isbn", columnNames = {"isbn"})
})
@Data
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String isbn;

    @Column(nullable = false)
    private String title;

    private String synopsis;

    @ManyToMany
    private List<Author> authors;
}
