package be.bstorm.formation.orsys2023ucljpa.entities;

import jakarta.persistence.*;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Data
public class Borrow {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
//    @EmbeddedId
//    private BorrowPK id;

    @ManyToOne
    private Customer customer;

    @ManyToOne
    private Book book;

    @Column(nullable = false)
    private LocalDate startAt;
    private LocalDate endAt;

//    @Embeddable
//    @Data
//    public static class BorrowPK implements Serializable {
//        private Long customerId;
//        private Long bookId;
//    }
}
