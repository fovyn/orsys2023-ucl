package be.bstorm.formation.orsys2023ucljpa.entities;

import jakarta.persistence.Entity;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
public class Author extends Person {
    private String pseudo;
}
