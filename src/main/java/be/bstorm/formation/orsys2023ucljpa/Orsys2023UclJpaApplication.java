package be.bstorm.formation.orsys2023ucljpa;

import be.bstorm.formation.orsys2023ucljpa.entities.security.User;
import be.bstorm.formation.orsys2023ucljpa.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
@EnableConfigurationProperties
@RequiredArgsConstructor
public class Orsys2023UclJpaApplication implements CommandLineRunner {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    public static void main(String[] args) {
        SpringApplication.run(Orsys2023UclJpaApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
//        User user = new User();
//        user.setUsername("Flavian");
//        user.setPassword(passwordEncoder.encode("blop"));
//
//        this.userRepository.save(user);
    }
}
