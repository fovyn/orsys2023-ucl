package be.bstorm.formation.orsys2023ucljpa.models.security;

public record Token(
        String value
) {
}
