package be.bstorm.formation.orsys2023ucljpa.models.security;

import jakarta.validation.constraints.NotBlank;

public record LoginDto(
        @NotBlank(message = "error.validation.notBlank") String username,
        @NotBlank(message = "error.validation.notBlank") String password
) {
}
